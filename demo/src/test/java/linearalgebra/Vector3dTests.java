//Jack Hannan 2037915
package linearalgebra;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.nio.file.FileAlreadyExistsException;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class Vector3dTests
{
    //creating object
    Vector3d testVec = new Vector3d(1,2,3);

    //Get-method() test
    @Test
    public void getMethodTest()
    {
        //creating object
        Vector3d testVec = new Vector3d(1,2,3);
        //testing get-methods
        //X
        assertTrue(testVec.getX() == 1);
        //Y
        assertTrue(testVec.getY() == 2);
        //Z
        assertTrue(testVec.getZ() == 3);
    }

    //Magnitude() test
    @Test
    public void magnitudeTest()
    {
        //creating object
        Vector3d testVec = new Vector3d(1,2,3);
        double magnitudeResult = Math.sqrt(1+4+9);
        assertTrue(testVec.magnitude() == magnitudeResult);
    }

    //Dot-Product() test
    @Test
    public void dotProductTest(){
        //creating objects
        Vector3d testVec = new Vector3d(1,2,3);
        Vector3d testVec2 = new Vector3d(2,4,6);
        double dotProduct = testVec.dotProduct(testVec2);
        assertTrue(dotProduct == 28);
    }

    //Add-method() test
    @Test
    public void addTest(){
        //creating objects
        Vector3d testVec = new Vector3d(1,2,3);
        Vector3d testVec2 = new Vector3d(2, 4, 6);
        //Using add to make more Vector3ds
        Vector3d addResult = testVec.add(testVec2);
        Vector3d addExpected = new Vector3d(3, 6, 9);
        //Comparing all values
        assertTrue(addResult.getX() == addExpected.getX());
        assertTrue(addResult.getY() == addExpected.getY());
        assertTrue(addResult.getZ() == addExpected.getZ());
    }
}
