//Jack Hannan 2037915

package linearalgebra;

public class Vector3d {

    //fields
    private double x;
    private double y;
    private double z;

    //constructor
    public Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    //get-methods
    public double getX(){
        return this.x;
    }
    public double getY(){
        return this.y;
    }
    public double getZ(){
        return this.z;
    }

    //additional instance methods
    //magnitude
    public double magnitude(){
        return Math.sqrt((x*x) + (y*y) + (z*z));
    }
    //dotProduct
    public double dotProduct(Vector3d secondVec){
        return (this.x * secondVec.getX()) + (this.y * secondVec.getY()) + (this.z * secondVec.getZ());
    }
    //add
    public Vector3d add(Vector3d secondVec){
        return new Vector3d(this.x + secondVec.getX(), this.y + secondVec.getY(), this.z + secondVec.getZ());
    }
}
